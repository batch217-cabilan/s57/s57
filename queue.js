let collection = [];
// Write the queue functions below.

function print(){

    return collection;

}

function enqueue(john){


    collection.push(john);
    return collection;
}


function dequeue(jane){

    collection.shift(jane)
    return collection;

}


function front(){

    let firstElement = collection[0];
    return firstElement;

}


function size(){
    let size = collection.length;
    return size;

}

function isEmpty(){

    let queue = collection.length;

    if(queue === 0){
        return true
    }else{
        return false
    }

}


// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};